package com.thouseef.contactlist;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.thouseef.contactlist.db.DatabaseAdapter;
import com.thouseef.contactlist.utils.CommonClass;
import com.thouseef.contactlist.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    DatabaseAdapter dbh;
    SQLiteDatabase db;
    CommonClass cc;
    String url="http://jsonplaceholder.typicode.com/users";
    //ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbh = new DatabaseAdapter(this);
        db = dbh.getWritableDatabase();
        cc = new CommonClass(this);

        if (savedInstanceState==null) {

            // check for contacts in database, if not available then get from server
            // if available the list down
            Cursor cur = db.rawQuery("SELECT * FROM contacts", null);
            if (cur.getCount() == 0 ) {
                if(cc.isOnline()) {
                    new getContactsFromServer().execute();
                }else{
                    cc.showAlertForInternet();
                }
            }else{
                populateContacts(null);
            }
            cur.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_delete:
                if( Constant.contact_id==0){
                    cc.showAlertWithTitle(getResources().getString(R.string.alert),"Please Select contact to delete.");
                }else{
                    db.delete("contacts","_id="+Constant.contact_id,null);
                    populateContacts(null);
                    Constant.contact_id=0;
                }

                break;
            // action with ID action_settings was selected
            case R.id.action_sortAZ:

                populateContacts("ASC");
                Constant.contact_id=0;
                break;

            case R.id.action_sortZA:

                populateContacts("DESC");
                Constant.contact_id=0;
                break;

            case R.id.action_refresh:
                new getContactsFromServer().execute();
                Constant.contact_id=0;
                break;
            default:
                break;
        }

        return true;
    }

    @SuppressLint("StaticFieldLeak")
    public class getContactsFromServer extends AsyncTask<Void, Void, Void> {
        ProgressDialog authProgressDialog;


        @Override
        protected void onPreExecute() {
            authProgressDialog = ProgressDialog.show(MainActivity.this, "", getResources().getString(R.string.loading_contcts), true, false);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            getContacts();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            authProgressDialog.dismiss();
            populateContacts(null);
        }
    }

     void getContacts() {

            String jsonStr =cc.oKHttpGet(url);

            if (jsonStr != null) {
                try {
                    JSONArray json = new JSONArray(jsonStr);
                    for(int i=0;i<json.length();i++){
                        JSONObject jObj = json.getJSONObject(i);

                        ContentValues values = new ContentValues();
                        //get common details
                        values.put("_id", jObj.getInt("id"));
                        values.put("name", jObj.getString("name"));
                        values.put("username", jObj.getString("username"));
                        values.put("email", jObj.getString("email"));

                        values.put("phone", jObj.getString("phone"));
                        values.put("website", jObj.getString("website"));

                        //get address details
                        JSONObject addressObj= jObj.getJSONObject("address");
                        values.put("address_street", addressObj.getString("street"));
                        values.put("address_suite", addressObj.getString("suite"));
                        values.put("address_city", addressObj.getString("city"));
                        values.put("address_zipcode", addressObj.getString("zipcode"));

                        //get company details
                        JSONObject companyObj= jObj.getJSONObject("company");
                        values.put("company_name", companyObj.getString("name"));
                        values.put("company_cp", companyObj.getString("catchPhrase"));
                        values.put("company_bs", companyObj.getString("bs"));

                        // insert into database
                        db.insert("contacts", null, values);

                        Log.e("inserted",companyObj.getString("name") );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

    }
     void populateContacts(String query){

         boolean isTablet= getResources().getBoolean(R.bool.isTablet);
         if(isTablet){ //for tab
             FragmentManager fm1 = getSupportFragmentManager();
             FragmentTransaction ft1 = fm1.beginTransaction();

             ContactFragment frg1 = new ContactFragment();
             Bundle b1 = new Bundle();
             b1.putString("query", query);
             frg1.setArguments(b1);

             ft1.replace(R.id.contactsFrame, frg1);
             ft1.commit();

             FragmentManager fm2 = getSupportFragmentManager();
             FragmentTransaction ft2 = fm2.beginTransaction();
             DetailFragment frg2 = new DetailFragment();
             ft2.replace(R.id.detailsFrame, frg2);
             ft2.commit();
         }else{ // for Mobile
             FragmentManager fm1 = getSupportFragmentManager();
             FragmentTransaction ft1 = fm1.beginTransaction();

             ContactFragment frg1 = new ContactFragment();
             Bundle b1 = new Bundle();
             b1.putString("query", query);
             frg1.setArguments(b1);
             ft1.replace(R.id.contactsFrame, frg1);
             ft1.commit();
         }
    }
}
