package com.thouseef.contactlist.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseAdapter extends SQLiteOpenHelper {

    public DatabaseAdapter(Context context) {
        super(context, "contacts.db", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String contacts = "CREATE TABLE contacts(" + "_id" + " INTEGER PRIMARY KEY ,"
                + "name" + " TEXT,"
                + "username" + " TEXT,"
                + "email" + " TEXT,"
                + "address_street" + " TEXT,"
                + "address_suite" + " TEXT,"
                + "address_city" + " TEXT,"
                + "address_zipcode" + " TEXT,"
                + "phone" + " TEXT,"
                + "website" + " TEXT,"
                + "company_name" + " TEXT,"
                + "company_cp" + " TEXT,"
                + "company_bs" + " TEXT" + ")";
        db.execSQL(contacts);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}