package com.thouseef.contactlist;


import android.app.ActionBar;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thouseef.contactlist.db.DatabaseAdapter;
import com.thouseef.contactlist.utils.CommonClass;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment {

    DatabaseAdapter dbh;
    SQLiteDatabase db;
    CommonClass cc;
    int contact_id = 0;

    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        setHasOptionsMenu(true);

        dbh = new DatabaseAdapter(getActivity());
        db = dbh.getWritableDatabase();
        cc = new CommonClass(getActivity());


        TextView lblName = rootView.findViewById(R.id.lblNameInDetail);
        TextView lblUserName = rootView.findViewById(R.id.lblUserNameInDetail);
        TextView lblPhone = rootView.findViewById(R.id.lblPhoneInDetail);
        TextView lblAddress = rootView.findViewById(R.id.lblAddressInDetail);
        TextView lblWebsite = rootView.findViewById(R.id.lblWebsiteInDetail);
        TextView lblCompany = rootView.findViewById(R.id.lblCompanyInDetail);


        TextView lblUserNameLbl = rootView.findViewById(R.id.lblUserNameLabelInDetail);
        TextView lblPhoneLbl = rootView.findViewById(R.id.lblPhoneLabelInDetail);
        TextView lblAddressLbl = rootView.findViewById(R.id.lblAddressLabelInDetail);
        TextView lblWebsiteLbl = rootView.findViewById(R.id.lblWebsiteLabelInDetail);
        TextView lblCompanyLbl = rootView.findViewById(R.id.lblCompanyLabelInDetail);


        Bundle b = this.getArguments();
        if (b != null) {
            contact_id = b.getInt("contact_id");
        }

        final Cursor cur = db.rawQuery("SELECT * FROM contacts WHERE _id=" + contact_id, null);
        if (cur.getCount() != 0) {
            cur.moveToFirst();

            String name = cur.getString(cur.getColumnIndex("name"));
            String username = cur.getString(cur.getColumnIndex("username"));
//            String email = cur.getString(cur.getColumnIndex("email"));
            String  address_street= cur.getString(cur.getColumnIndex("address_street"));
            String  address_suite= cur.getString(cur.getColumnIndex("address_suite"));
            String  address_city= cur.getString(cur.getColumnIndex("address_city"));
            String  address_zipcode= cur.getString(cur.getColumnIndex("address_zipcode"));
            String  phone= cur.getString(cur.getColumnIndex("phone"));
            String  website= cur.getString(cur.getColumnIndex("website"));
            String  company_name= cur.getString(cur.getColumnIndex("company_name"));
            String  company_cp= cur.getString(cur.getColumnIndex("company_cp"));
            String  company_bs= cur.getString(cur.getColumnIndex("company_bs"));


            lblUserNameLbl.setText(getResources().getString(R.string.username));
            lblPhoneLbl.setText(getResources().getString(R.string.phone));
            lblAddressLbl.setText(getResources().getString(R.string.address));
            lblWebsiteLbl.setText(getResources().getString(R.string.website));
            lblCompanyLbl.setText(getResources().getString(R.string.company));


            lblName.setText(name);
            lblUserName.setText(username);
            lblPhone.setText(phone);
            lblWebsite.setText(website);
            lblAddress.setText(address_street+"\n"+address_suite+"\n"+address_city+","+address_zipcode);
            lblCompany.setText(company_name+"\n"+company_cp+"\n"+company_bs);

        }
        cur.close();

        return rootView;
    }


    @Override
    public void onResume()
    {
        super.onResume();

        Objects.requireNonNull(getView()).setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK)
                {
                    boolean isTablet= getResources().getBoolean(R.bool.isTablet);
                    if(!isTablet) {
                        FragmentManager fm2 = getFragmentManager();
                        FragmentTransaction ft2 = fm2.beginTransaction();

                        ContactFragment frg2 = new ContactFragment();
                        ft2.replace(R.id.contactsFrame, frg2);
                        ft2.setCustomAnimations(R.anim.enter, R.anim.exit);
                        ft2.commit();
                        // handle back button's click listener
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        boolean isTablet= getResources().getBoolean(R.bool.isTablet);
        if(!isTablet) {
            menu.findItem(R.id.action_delete).setVisible(true);
            menu.findItem(R.id.action_sortAZ).setVisible(false);
            menu.findItem(R.id.action_sortZA).setVisible(false);
            menu.findItem(R.id.action_refresh).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
    }
}
