package com.thouseef.contactlist;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thouseef.contactlist.db.DatabaseAdapter;
import com.thouseef.contactlist.utils.CommonClass;
import com.thouseef.contactlist.utils.Constant;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends Fragment {

    DatabaseAdapter dbh;
    SQLiteDatabase db;
    CommonClass cc;

    LinearLayout llContacts;
    public ContactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_contact, container, false);
        setHasOptionsMenu(true);


        dbh = new DatabaseAdapter(getActivity());
        db = dbh.getWritableDatabase();
        cc = new CommonClass(getActivity());

        llContacts=rootView.findViewById(R.id.llContacts);

        EditText edtSearch=rootView.findViewById(R.id.edtSearch);

        // search option
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    populateContactList(null,editable.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        String query=null;
        Bundle b = this.getArguments();
        if (b != null) {
            query = b.getString("query");
        }
        populateContactList(query,null);

        return rootView;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        boolean isTablet= getResources().getBoolean(R.bool.isTablet);
        if(!isTablet) {
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_sortAZ).setVisible(true);
            menu.findItem(R.id.action_sortZA).setVisible(true);
            menu.findItem(R.id.action_refresh).setVisible(true);
        }
        super.onPrepareOptionsMenu(menu);
    }

    void populateContactList(String query,String Like){
        String rawquery = null;
        if(query!=null && Like==null){
            rawquery="SELECT * FROM contacts ORDER BY name "+query;
        }else if(query==null && Like==null){
            rawquery="SELECT * FROM contacts";
        }else if(query==null && Like!=null){
            rawquery="SELECT * FROM contacts WHERE name LIKE '%" + Like + "%'";
        }


        final Cursor cur=db.rawQuery(rawquery,null);
        if(cur.getCount()!=0){
            llContacts.removeAllViews();
            cur.moveToFirst();
            do {
                final View childView = getLayoutInflater().inflate(R.layout.item_contacts, null);

                final TextView lblName = childView.findViewById(R.id.lblFirstName);
                final TextView lblEmail= childView.findViewById(R.id.lblEmail);

                String name=cur.getString(cur.getColumnIndex("name"));
                String email=cur.getString(cur.getColumnIndex("email"));
                final int contact_id=cur.getInt(cur.getColumnIndex("_id"));

                lblName.setText(name);
                lblEmail.setText(email);

                childView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        boolean isTablet= getResources().getBoolean(R.bool.isTablet);
                        if(isTablet){
                            FragmentManager fm2 = getFragmentManager();
                            FragmentTransaction ft2 = fm2.beginTransaction();

                            DetailFragment frg2 = new DetailFragment();
                            Bundle b1 = new Bundle();
                            b1.putInt("contact_id", contact_id);
                            Constant.contact_id = contact_id;
                            frg2.setArguments(b1);
                            ft2.replace(R.id.detailsFrame, frg2);
                            ft2.commit();
                        }else {
                            FragmentManager fm2 = getFragmentManager();
                            FragmentTransaction ft2 = fm2.beginTransaction();

                            DetailFragment frg2 = new DetailFragment();
                            Bundle b1 = new Bundle();
                            b1.putInt("contact_id", contact_id);
                            Constant.contact_id = contact_id;
                            frg2.setArguments(b1);
                            ft2.replace(R.id.contactsFrame, frg2);
                            ft2.commit();
                        }
                    }
                });


                llContacts.addView(childView);

            }while (cur.moveToNext());
        }cur.close();
    }
}
